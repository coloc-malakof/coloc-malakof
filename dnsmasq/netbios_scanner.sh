#!/bin/sh

NETBIOS_NETWORK=${NETBIOS_NETWORK:=10.11.12.0/24}
NETBIOS_NAME_SUFFIX=${NETBIOS_NAME_SUFFIX:=.local}
NETBIOS_SCAN_INTERVAL=${NETBIOS_SCAN_INTERVAL:=600}

while true; do
    # do a netbios scan, append suffix and save it to a file
    nbtscan -t 3 -e -q $NETBIOS_NETWORK > /etc/hosts.netbios

    # wait for next scan
    sleep $NETBIOS_SCAN_INTERVAL
done