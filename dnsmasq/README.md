# Dnsmasq

## Introduction

Dnsmasq is the DNS/DHCP server.

## Additional configuration

Create a file `/etc/network/interfaces.d/mac0` with the following content where
`enp3s0` is your primary interface and `00:11:22:33:44:55` the fake MAC address
assigned to the `mac0` interface:

```
auto mac0
iface mac0 inet static
    address 10.11.12.2/24
    gateway 10.11.12.1
    pre-up ip link add link enp3s0 address 00:11:22:33:44:55 name mac0 type macvlan mode bridge
```

Then, restart the network service with the following command:

```sh
sudo systemctl restart networking
```

**Notes**: The primary interface must be set without an IP as the "new" virtual
interface is bridged the physical interface. If not, a default route will be
created with the physical interface and the host will not be able to resolve DNS
names.

## Additional resources

- [Dnsmasq](https://thekelleys.org.uk/dnsmasq/doc.html) - DNS, DHCP, router
  advertisement and network boot (TFTP, BOOTP, PXE).
- andyshinn/dnsmasq (outdated) -> drpsychick/dnsmasq
- https://whi.tw/ell/blog/docker-compose-traefik-dnsmasq/
- https://github.com/thomaschampagne/masquarade
- https://github.com/notracking/hosts-blocklists
- foxcris/docker-dnsmasq
- liip/docker-dnsmasq
- tschaffter/docker-dnsmasq
- https://github.com/metal3d/docker-auto-dnsmasq
- https://github.cdnweb.icu/moonbuggy/docker-dnsmasq-updater
- [Pi-hole](https://github.com/pi-hole/docker-pi-hole)
- https://github.com/blu3r4y/docker-dnsmasq-netbios
