# Dolibarr

## Introduction

ERP & CRM

One web suite to manage all of your business...

## FQDN and HTTPS

You might want to set your own custom FQDN in the `.env` file.

To enable HTTPS, uncomment the related configuration in the
`docker-compose.yaml` file.

## Additional configuration

_None_

## Additional resources

- [Dolibarr](https://www.dolibarr.org/?lang=fr&l=fr)
- [docker-dolibarr](https://hub.docker.com/r/tuxgasy/dolibarr/)
