# File Browser

> filebrowser provides a file managing interface within a specified directory
> and it can be used to upload, delete, preview, rename and edit your files. It
> allows the creation of multiple users and each user can have its own
> directory. It can be used as a standalone app.
>
> <https://filebrowser.org/>

## Prerequisites

The following prerequisites must be filled to run this service:

- [Docker](https://docs.docker.com/get-docker/) must be installed.
- [Docker Compose](https://docs.docker.com/compose/install/) must be installed
  (it should be installed by default with Docker in most cases).

## Set the environment variables

Edit the `*.env` files to your needs.

## Additional configuration

You must create the `config/database.db` file prior to run the container.

```sh
# Create the `config` directory
mkdir config

# Create the database file
touch config/database.db
```

## Run the application with Docker

Do not forget to set the environment variables as described in the previous
section.

In a terminal, run the following commands:

```bash
# Pull the latest images
docker compose pull

# Start the application with Docker
docker compose up --detach
```

## Additional resources

- [File Browser](https://filebrowser.org/)
