# Rust Dedicated Server

## Introduction

Official Rust Dedicated Server.

## Prerequisites

The following prerequisites must be filled to run this service:

- [Docker](https://docs.docker.com/get-docker/) must be installed.
- [Docker Compose](https://docs.docker.com/compose/install/) must be installed
  (it should be installed by default with Docker in most cases).

## Set the environment variables

Edit the `.env` file to your needs.

## Run the application with Docker

You can now start the application with Docker either in online or offline mode:

- Online mode: the server will download the latest version of the game and
  update it. This must be ran at least once to download the game files (this can
  take a few minutes depending on your connection).
- Online mode with a specific version: the server will download the specified
  version of the game. This must be ran at least once to download the game files
  (**this can take many hours**). This will guarantee that the client and server
  will always run the same compatible version of the game.
- Offline mode: the server will use the game files that are already present in
  the Docker volume directories described in the `.gitignore` file.

In both cases, the server will be started in LAN mode.

Do not forget to set the environment variables as described in the previous
section.

In a terminal, run the following commands:

```bash
# To download a specific version of the game, enable this override file
cp docker-compose.override.download-exact-version.yaml docker-compose.override.yaml

# Download the client at least once in online mode to download the game files
# Set the Steam username and password in the .env file if needed
docker compose run -it --rm download-rust

# Download the server at least once in online mode to download the game files
docker compose run --rm download-rust-dedicated-server

# In offline mode, enable this override file
cp docker-compose.override.offline.yaml docker-compose.override.yaml

# Start the dedicated server
docker compose up --detach run-rust-dedicated-server

# Attach to the logs
docker compose logs --follow

# Attach to a container (e.g. to run commands) - type `Ctrl + P` and `Ctrl + Q` to leave the container without stopping SteamCMD
docker compose attach run-rust-dedicated-server

# Stop the dedicated server with Docker
docker compose down
```

## Resources

- <https://developer.valvesoftware.com/wiki/SteamCMD>
- <https://developer.valvesoftware.com/wiki/Command_line_options>
- <https://developer.valvesoftware.com/wiki/Rust_Dedicated_Server>
