# Coloc Malakof

[![License][license-badge]][license-url]

## Introduction

The Coloc Malakof was a shared flat made up of four young people loving computer
projects mostly regarding the NAS they had at the time. They still maintain
various projects regarding the NAS.

Even after the dissolution of the shared flat, the name stayed.

## Architecture

A proxmox server is used to host various containers.

The proxmox server as well as the containers are managed by
[Ansible](./ansible/README.md).

```plantuml
@startuml
!include <office/Devices/router>
!include <office/Devices/workstation>
!include <office/Servers/cluster_server>

nwdiag {
  internet [shape = cloud];
  router [address = "<external ip>",  description = "<$router>\n router"];
  proxmox [description = "<$cluster_server>\nproxmox\n(server)"];
  libreelec [description = "<$workstation>\nlibreelec\n(computer)"];

  internet -- router

  network internal {
      address = "10.11.12.x/24"

      router [address = "10.11.12.1"];
      libreelec [address = "10.11.12.9"];
      proxmox [address = "10.11.12.10"];
  }
}
@enduml
```

### `proxmox.local`

```plantuml
@startuml
!include <office/Devices/router>
!include <office/Servers/virtual_application_server>
!include <office/Servers/cluster_server>

nwdiag {
  router [address = "10.11.12.1",  description = "<$router>\n router"];
  proxmox [description = "<$cluster_server>\nproxmox\n(server)"];
  nas [description = "<$virtual_application_server>\nnas\n(container)"];
  ludelafo [description = "<$virtual_application_server>\nludelafo\n(container)"];
  matthieu [description = "<$virtual_application_server>\nmatthieu\n(container)"];
  yannis [description = "<$virtual_application_server>\nyannis\n(container)"];

  router -- proxmox

  network internal {
      address = "10.11.12.x/24"

      proxmox [address = "10.11.12.10"];
      nas [address = "10.11.12.2"];
      ludelafo [address = "10.11.12.3"];
      matthieu [address = "10.11.12.4"];
      yannis [address = "10.11.12.5"];
  }
}
@enduml
```

### `nas.local`

```plantuml
@startuml
!include <office/Concepts/application_generic>
!include <office/Servers/virtual_application_server>
!include <office/Servers/cluster_server>
!include <office/Servers/file_server>

nwdiag {
  proxmox [address = "10.11.12.10", description = "<$cluster_server>\nproxmox\n(server)"];
  nas [description = "<$virtual_application_server>\nnas\n(container)"];
  homepage [description = "<$application_generic>\nHomepage\n(nas.local)"];
  traefik [description = "<$application_generic>\nTraefik\n(traefik.local)"];
  samba [description = "<$application_generic>\nSamba\n(\\\\10.11.12.2)"];
  wireguard [description = "<$application_generic>\nWireGuard\n"];

  proxmox -- nas

  network nas.local {
      address = "10.11.12.2"

      nas
      homepage
      traefik
      wireguard
      samba
  }
}
@enduml
```

### `ludelafo.local`

```plantuml
@startuml
!include <office/Concepts/application_generic>
!include <office/Servers/virtual_application_server>
!include <office/Servers/cluster_server>
!include <office/Servers/file_server>

nwdiag {
  proxmox [address = "10.11.12.10", description = "<$cluster_server>\nproxmox\n(server)"];
  ludelafo [description = "<$virtual_application_server>\nludelafo\n(container)"];
  homepage [description = "<$application_generic>\nHomepage\n(ludelafo.local)"];
  traefik [description = "<$application_generic>\nTraefik\n(traefik.ludelafo.local)"];
  samba [description = "<$application_generic>\nSamba\n(\\\\10.11.12.3)"];

  proxmox -- ludelafo

  network ludelafo.local {
      address = "10.11.12.3"

      ludelafo
      homepage
      traefik
      samba
  }
}
@enduml
```

### `matthieu.local`

```plantuml
@startuml
!include <office/Concepts/application_generic>
!include <office/Servers/virtual_application_server>
!include <office/Servers/cluster_server>
!include <office/Servers/file_server>

nwdiag {
  proxmox [address = "10.11.12.10", description = "<$cluster_server>\nproxmox\n(server)"];
  matthieu [description = "<$virtual_application_server>\nmatthieu\n(container)"];
  homepage [description = "<$application_generic>\nHomepage\n(matthieu.local)"];
  traefik [description = "<$application_generic>\nTraefik\n(traefik.matthieu.local)"];
  samba [description = "<$application_generic>\nSamba\n(\\\\10.11.12.4)"];

  proxmox -- matthieu

  network matthieu.local {
      address = "10.11.12.4"

      matthieu
      homepage
      traefik
      samba
  }
}
@enduml
```

### `yannis.local`

```plantuml
@startuml
!include <office/Concepts/application_generic>
!include <office/Servers/virtual_application_server>
!include <office/Servers/cluster_server>
!include <office/Servers/file_server>

nwdiag {
  proxmox [address = "10.11.12.10", description = "<$cluster_server>\nproxmox\n(server)"];
  yannis [description = "<$virtual_application_server>\nyannis\n(container)"];
  homepage [description = "<$application_generic>\nHomepage\n(yannis.local)"];
  traefik [description = "<$application_generic>\nTraefik\n(traefik.yannis.local)"];
  samba [description = "<$application_generic>\nSamba\n(\\\\10.11.12.5)"];

  proxmox -- yannis

  network yannis.local {
      address = "10.11.12.5"

      yannis
      homepage
      traefik
      samba
  }
}
@enduml
```

## Documentation

### Prerequisites

The following prerequisites must be filled to run this project:

- [Docker](https://docs.docker.com/get-docker/) must be installed.
- [Docker Compose](https://docs.docker.com/compose/) must be installed.
- [Git](https://git-scm.com/) must be installed.

### Clone the repository

```sh
# Clone the repository
git clone https://gitlab.com/coloc-malakof/coloc-malakof.git apps
```

### Configure the application(s)

```sh
# For each app, edit the environment variables files if needed
vim <app>/*.env

# Other configuration might be required in the app's README.md
cat <app>/README.md
```

### Start the application(s)

```sh
# Start the app(s)
docker compose --project-directory <app> --up
```

### Stop the application(s)

```sh
# Stop the app(s)
docker compose --project-directory <app> --down
```

### Update the application(s)

Fetch the latest available version of the Docker images from the remote
registry.

```sh
# Update the app(s)
docker compose --project-directory <app> --pull
```

### Display the application(s) logs

```sh
# Display the app(s) logs
docker compose --project-directory <app> logs [--follow] [--tail="all"]
```

## License

The source code is licensed under the MIT License - see the
[`LICENSE`][license-url] file for details.

[license-badge]: https://img.shields.io/badge/license-MIT-blue.svg
[license-url]: LICENSE
