# Ansible

## Generic

```sh
# Fetch the latest package lists
apt update

# Install Ansible and Git
apt install --yes ansible git

# Execute Ansible pull
ansible-pull --url https://gitlab.com/coloc-malakof/coloc-malakof ansible/playbooks/proxmox.yaml --extra-vars "@.ansible/pull/<hostname>/ansible/variables/nas.yaml"

# or
ansible-pull --url https://gitlab.com/coloc-malakof/coloc-malakof ansible/playbook.yaml --extra-vars "user=nas user_id=1000 group=nas group_id=1000"

# or
ansible-pull --url https://gitlab.com/coloc-malakof/coloc-malakof ansible/playbook.yaml --extra-vars "{\"user\": \"nas\", \"user_id\": 1000, \"group\": \"nas\", \"group_id\": 1000}"

# or
ansible-pull --url https://gitlab.com/coloc-malakof/coloc-malakof --checkout <name of the branch> ansible/playbooks/fonkylan.yaml --extra-vars "@.ansible/pull/<hostname>/ansible/variables/fonkylan.yaml"
```

## On proxmox host

```sh
# Fetch the latest package lists
apt update

# Install Ansible and Git
apt install --yes ansible git

# Execute Ansible pull
ansible-pull --url https://gitlab.com/coloc-malakof/coloc-malakof ansible/playbooks/proxmox.yaml --extra-vars "@.ansible/pull/proxmox.local/ansible/variables/nas.yaml"

ansible-pull --url https://gitlab.com/coloc-malakof/coloc-malakof ansible/playbooks/proxmox.yaml --extra-vars "@.ansible/pull/proxmox.local/ansible/variables/ludelafo.yaml"

ansible-pull --url https://gitlab.com/coloc-malakof/coloc-malakof ansible/playbooks/proxmox.yaml --extra-vars "@.ansible/pull/proxmox.local/ansible/variables/matthieu.yaml"

ansible-pull --url https://gitlab.com/coloc-malakof/coloc-malakof ansible/playbooks/proxmox.yaml --extra-vars "@.ansible/pull/proxmox.local/ansible/variables/yannis.yaml"
```

## On nas host

```sh
# Fetch the latest package lists
apt update

# Install Ansible and Git
apt install --yes ansible git

# Execute Ansible pull
ansible-pull --url https://gitlab.com/coloc-malakof/coloc-malakof ansible/playbooks/container.yaml --extra-vars "@.ansible/pull/nas.local/ansible/variables/nas.yaml"

# Set the password for user
passwd nas

# Log out and log in again with the new user
logout

# Delete the root home
sudo rm -rf /root
```

## On ludelafo host

```sh
# Fetch the latest package lists
apt update

# Install Ansible and Git
apt install --yes ansible git

# Execute Ansible pull
ansible-pull --url https://gitlab.com/coloc-malakof/coloc-malakof ansible/playbooks/container.yaml --extra-vars "@.ansible/pull/ludelafo.local/ansible/variables/ludelafo.yaml"

# Set the password for user
passwd ludelafo

# Log out and log in again with the new user
logout

# Delete the root home
sudo rm -rf /root
```

## On matthieu host

```sh
# Fetch the latest package lists
apt update

# Install Ansible and Git
apt install --yes ansible git

# Execute Ansible pull
ansible-pull --url https://gitlab.com/coloc-malakof/coloc-malakof ansible/playbooks/container.yaml --extra-vars "@.ansible/pull/matthieu.local/ansible/variables/matthieu.yaml"

# Set the password for user
passwd matthieu

# Log out and log in again with the new user
logout

# Delete the root home
sudo rm -rf /root
```

## On yannis host

```sh
# Fetch the latest package lists
apt update

# Install Ansible and Git
apt install --yes ansible git

# Execute Ansible pull
ansible-pull --url https://gitlab.com/coloc-malakof/coloc-malakof ansible/playbooks/container.yaml --extra-vars "@.ansible/pull/yannis.local/ansible/variables/yannis.yaml"

# Set the password for user
passwd yannis

# Log out and log in again with the new user
logout

# Delete the root home
sudo rm -rf /root
```

On fonkylan host

```sh
# Fetch the latest package lists
apt update

# Install Ansible and Git
apt install --yes ansible git

# Execute Ansible pull
ansible-pull --url https://gitlab.com/coloc-malakof/coloc-malakof ansible/playbooks/container.yaml --extra-vars "@.ansible/pull/fonkylan.local/ansible/variables/fonkylan.yaml"

# Set the password for user
passwd fonkylan

# Log out and log in again with the new user
logout

# Delete the root home
sudo rm -rf /root
```
