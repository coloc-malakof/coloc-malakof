# wg-easy

> The easiest way to run WireGuard VPN + Web-based Admin UI.
>
> <https://github.com/wg-easy/wg-easy>

## Prerequisites

The following prerequisites must be filled to run this service:

- [Docker](https://docs.docker.com/get-docker/) must be installed.
- [Docker Compose](https://docs.docker.com/compose/install/) must be installed
  (it should be installed by default with Docker in most cases).

## Set the environment variables

Edit the `*.env` files to your needs.

## Additional configuration

Add route 10.13.13.0/24 to 10.11.12.2 (nas LXC) on router
[10.11.12.1](http://10.11.12.1/#static_route_table) and on proxmox in
`/etc/network/interfaces` file add the end of the bridge interface `vmbr0`
config `up ip route add 10.13.13.0/24 via 10.11.12.2` then restart with
`sudo ifreload -a`

## Run the application with Docker

Do not forget to set the environment variables as described in the previous
section.

In a terminal, run the following commands:

```bash
# Pull the latest images
docker compose pull

# Start the application with Docker
docker compose up --detach
```

## Additional resources

- [WireGuard](https://www.wireguard.com/)
- [wg-easy](https://github.com/wg-easy/wg-easy)
