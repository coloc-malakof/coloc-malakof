# AdGuard Home

> AdGuard Home is a network-wide software for blocking ads and tracking. After
> you set it up, it'll cover all your home devices, and you won't need any
> client-side software for that.
>
> <https://adguard.com/adguard-home.html>

## Prerequisites

The following prerequisites must be filled to run this service:

- [Docker](https://docs.docker.com/get-docker/) must be installed.
- [Docker Compose](https://docs.docker.com/compose/install/) must be installed
  (it should be installed by default with Docker in most cases).

## Set the environment variables

Edit the `*.env` files to your needs.

## Additional configuration

By default, containers using mcvlan type Docker network and the host cannot
communicate together. We need to add a bridge between the host and the container
to make it work.

Install ifupdown2 on the host (`nas` host or `fonkylan` host) with the follwing
command:

```sh
# Install ifupdown2
sudo apt install --yes ifupdown2
```

Update `/etc/network/interfaces` add following lines at the end of the file as
shown below:

```text
auto eth0
iface eth0 inet static
        address 10.11.12.7/24 // Update with the IP address of the host
        gateway 10.11.12.1
        post-up ip link add host_macvlan link eth0 type macvlan mode bridge
        post-up ip link set host_macvlan up
        post-up ip route add 10.11.12.254/32 dev host_macvlan
```

Restart all the network interfaces with the following command:

```sh
# Restart all the network interfaces
sudo ifreload --all
```

Then you can start the AdGuard Home container.

On first run, you must access the web interface to configure AdGuard Home at
`10.11.12.254:3000`.

Once the configuration is done, you can access the web interface on the port you
set during the configuration (`10.11.12.254:80` by default).

## Run the application with Docker

Do not forget to set the environment variables as described in the previous
section.

In a terminal, run the following commands:

```bash
# Pull the latest images
docker compose pull

# Start the application with Docker
docker compose up --detach
```

## Additional resources

- [AdGuard Home](https://adguard.com/adguard-home.html)
- [_"Docker host can’t access containers running on macvlan"_ by Karlo Abaga / 2021-11-05](https://www.networkshinobi.com/docker-host-cant-access-containers-running-on-macvlan/)
