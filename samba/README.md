# Samba

> Samba is an Open Source / Free Software suite that has, since 1992, provided
> file and print services to all manner of SMB/CIFS clients, including the
> numerous versions of Microsoft Windows operating systems.
>
> <https://www.samba.org/>

## Prerequisites

The following prerequisites must be filled to run this service:

- [Docker](https://docs.docker.com/get-docker/) must be installed.
- [Docker Compose](https://docs.docker.com/compose/install/) must be installed
  (it should be installed by default with Docker in most cases).

## Set the environment variables

Edit the `*.env` files to your needs.

## Additional configuration

You must create a `data/config.yml` file prior to run the container. You can
check examples in the [`data`](./data) directory.

You might need to update the `volumes` section in the `docker-compose.yaml` file
to match your configuration.

## Run the application with Docker

Do not forget to set the environment variables as described in the previous
section.

In a terminal, run the following commands:

```bash
# Pull the latest images
docker compose pull

# Start the application with Docker
docker compose up --detach
```

## Additional resources

- [Samba](https://www.samba.org/)
- [docker-samba](https://github.com/crazy-max/docker-samba)
