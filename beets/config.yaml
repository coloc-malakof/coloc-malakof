## https://beets.readthedocs.io/en/stable/index.html

# Where to keep the database
library: /config/beets.db

# Where to import the music
directory: /data/media/music

# Import options
import:
  # Do not edit the path. The filename can be changed.
  log: /config/beets.log
  write: yes
  hardlink: no
  copy: no
  move: yes
  incremental: no
  resume: ask
  detail: yes
  autotag: yes
  duplicate_action: ask
  none_rec_action: skip
  bell: yes
  timid: yes

match:
  max_rec:
    missing_tracks: strong
    unmatched_tracks: strong

item_fields:
  my_multidisc: 1 if disctotal > 1 else 0

  my_discnumber: u'%i' % disc

  my_source: |
    if media == 'Digital Media':
      return 'WEB'
    elif media == 'Enhanced CD':
      return 'CD'
    elif media == '':
      return False
    else:
      return media

  my_samplerate: |
    if format == "FLAC":
      return round(int(samplerate) / 1000, 1)

  my_bitdepth: |
    if format == "FLAC":
      return bitdepth

  my_year: |
    if year == original_year:
      return year
    else:
      return '{}, Reissue {}'.format(original_year,year)

album_fields:
  my_format: |
    average_bitrate = sum([item.bitrate for item in items]) / len(items)

    if average_bitrate > 480:
      return 'FLAC'
    elif average_bitrate < 480 and average_bitrate >= 320:
      return 'MP3 320'
    elif average_bitrate < 320 and average_bitrate >= 220:
      return 'MP3 V0'
    elif average_bitrate < 215 and average_bitrate >= 170 and average_bitrate != 192:
      return 'MP3 V2'
    elif average_bitrate == 192:
      return 'MP3 192'
    elif average_bitrate < 170:
      return 'MP3 %i' % average_bitrate

paths:
  default: $albumartist/$albumartist - $album%if{$albumdisambig, - %title{$albumdisambig}} ($my_year) [$my_format%if{$my_bitdepth && $my_samplerate, $my_bitdepth-$my_samplerate}%if{$my_source, $my_source}]%if{$catalognum, {$catalognum$}}/%if{$my_multidisc,CD $my_discnumber/}$track. $title

format_item: $albumartist - $album%if{$albumdisambig, - %title{$albumdisambig}} (%if{$original_year,$original_year,$year}) - $track. $title

format_album: $albumartist - $album%if{$albumdisambig, - %title{$albumdisambig}} (%if{$original_year,$original_year,$year})

sort_item: $albumartist+ $album+ $albumdisambig+ $year+ $disc+ $track+ $title+

sort_album: $albumartist+ $album+ $albumdisambig+ $year+ $disc+

sort_case_insensitive: yes

per_disc_numbering: yes

# If the process is threaded
threaded: yes

# The musicbrainz host
musicbrainz:
  host: musicbrainz.org
  ratelimit: 1
  searchlimit: 3

# UI customization
ui:
  color: yes
  colors:
    text_success: green
    text_warning: yellow
    text_error: red
    text_highlight: red
    text_highlight_minor: lightgray
    action_default: turquoise
    action: blue

art_filename: cover

aunique:
  keys: albumartist year album
  disambiguators: albumtype year label catalognum albumdisambig releasegroupdisambig
  bracket: '[]'

ignore: [
  ## Linux

  '*~',

  # Linux trash folder which might appear on any partition or disk
  '.Trash-*',

  ## macOS

  # General
  '.DS_Store',

  # Thumbnails
  '._*',

  # Files that might appear in the root of a volume
  '.DocumentRevisions-V100',
  '.fseventsd',
  '.Spotlight-V100',
  '.TemporaryItems',
  '.Trashes',
  '.VolumeIcon.icns',
  '.com.apple.timemachine.donotpresent',

  ## Windows

  # Windows thumbnail cache files
  'Thumbs.db',
  'Thumbs.db:encryptable',
  'ehthumbs.db',
  'ehthumbs_vista.db',

  # Folder config file
  '[Dd]esktop.ini',

  # Recycle Bin used on file shares
  '$RECYCLE.BIN/',

  # Windows shortcuts
  '*.lnk',
]

ignore_hidden: yes

clutter: [
  'Thumbs.DB',
  '.DS_Store',
]

asciify_paths: false

replace:
  '[\\/]': '_'
  '^\.': '_'
  '[\x00-\x1f]': '_'
  '[<>:"\?\*\|]': '_'
  '\.$': '_'
  '\s+$': '_'
  '^\s+': '_'
  '^-': '_'
  '’': "'"
  '[“”]': '"'
  '[\xE8-\xEB]': e
  '[\xEC-\xEF]': i
  '[\xE2-\xE6]': a
  '[\xF2-\xF6]': o
  '[\xF8]': o

max_filename_length: 180

va_name: 'Various Artists'

# Plugins
plugins: [
  # Autotagger
  chroma,
  # discogs,
  spotify,
  deezer,
  fromfilename,
  # Metadata
  # absubmit,
  # acousticbrainz,
  edit,
  fetchart,
  ftintitle,
  lastgenre,
  # Path Formats
  inline,
  # Miscellaneous
  ihate,
  web,
]

edit:
  itemfields: [
    track,
    title,
    artist,
    album,
  ]
  albumfields: [
    album,
    albumartist,
  ]

fetchart:
  auto: yes
  cautious: yes
  minwidth: 1000
  maxwidth: 1000
  enforce_ratio: yes
  sources: [
    filesystem,
    coverart,
    itunes,
    amazon,
    albumart,
    wikipedia,
  ]
  quality: 75

ftintitle:
  auto: yes
  drop: no
  format: '(feat. {0})'

hook:
  hooks:
  - event: write
    command: echo 'Processing "{item.path}"...'

  - event: album_imported
    command: echo 'Album "{album.path}" imported.'

ihate:
  warn: [
    'albumtype:live',
  ]
  skip: []

lastgenre:
  auto: yes
  count: 1
  fallback: ''
  force: no
  prefer_specific: no
  souce: album
  title_case: yes

web:
  host: 0.0.0.0
  port: 8337
