# Beets

## Introduction

Beets helps to manage our music collection.

## Additional configuration

Move the `config.yaml` file to the `/config/config.yaml` Docker volume described
in the `docker-compose.yaml` file.

Then, you can run the service with the following command:

```sh
docker-compose run --rm beets /usr/bin/beet import /data/downloads/complete/redacted
```

## Additional resources

- [Beets](https://beets.io/) - The music geek's media organizer.
