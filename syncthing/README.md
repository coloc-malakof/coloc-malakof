# Syncthing

> Syncthing is a continuous file synchronization program. It synchronizes files
> between two or more computers in real time, safely protected from prying eyes.
> Your data is your data alone and you deserve to choose where it is stored,
> whether it is shared with some third party, and how it’s transmitted over the
> internet.
>
> <https://syncthing.net/>

## Set the environment variables

Edit the `*.env` files to your needs.

## Additional configuration

_None_

## Run the application with Docker

Do not forget to set the environment variables as described in the previous
section.

In a terminal, run the following commands:

```bash
# Pull the latest images
docker compose pull

# Start the application with Docker
docker compose up --detach
```

## Additional resources

- [Syncthing](https://syncthing.net/)
