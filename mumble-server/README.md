# Mumble

> Mumble is a free, open source, low latency, high quality voice chat
> application.
>
> Mumble was the first VoIP application to establish true low latency voice
> communication over a decade ago. But low latency and gaming are not the only
> use cases it shines in.
>
> <https://www.mumble.info/>

## Prerequisites

The following prerequisites must be filled to run this service:

- [Docker](https://docs.docker.com/get-docker/) must be installed.
- [Docker Compose](https://docs.docker.com/compose/install/) must be installed
  (it should be installed by default with Docker in most cases).

## Set the environment variables

Edit the `*.env` files to your needs.

## Additional configuration

You must create the `data` directory prior to run the container.

```sh
# Create `data` directory
mkdir data
```

## Run the application with Docker

Do not forget to set the environment variables as described in the previous
section.

In a terminal, run the following commands:

```bash
# Pull the latest images
docker compose pull

# Start the application with Docker
docker compose up --detach
```

## Additional resources

- [Mumble](https://www.mumble.info/)
