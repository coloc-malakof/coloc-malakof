# Factorio Server

## Introduction

Vanilla Factorio Server.

## Prerequisites

The following prerequisites must be filled to run this service:

- [Docker](https://docs.docker.com/get-docker/) must be installed.
- [Docker Compose](https://docs.docker.com/compose/install/) must be installed
  (it should be installed by default with Docker in most cases).

## Set the environment variables

Edit the `*.env` files to your needs.

## Additional configuration

Start the server at least once to generate the necessary files:

```sh
# Start the server
docker compose run --rm factorio-server
```

Edit the `/data/config/server-settings.json` file to your needs but do not
forget to keep the `visibility` field to `lan` to allow the server to be
discovered on the local network:

```json
{
  ...

  "visibility":
  {
    "public": false,
    "lan": true
  },

  ...

  "require_user_verification": false,

  ...
}
```

## Run the application with Docker

Do not forget to set the environment variables as described in the previous
section.

In a terminal, run the following commands:

```bash
# Pull the latest images
docker compose pull

# Start the application with Docker
docker compose up --detach
```

## Additional resources

- <https://www.minecraft.net/>
- <https://github.com/itzg/docker-minecraft-server>
